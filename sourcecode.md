# Source code to Markdown
This file is automatically created by a script. Please delete this line and replace with the course and your team information accordingly.
## /editpost.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';

	$postid = sanitize_input($_POST["postid"]);
	$content = sanitize_input($_POST["content"]); // new content
	$currentUser = sanitize_input($_SESSION["username"]);
	$nocsrftoken = $_POST["nocsrftoken"];
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=form.php");
		die();
	}

	if (empty($postid) || empty($content) || !isset($postid) || !isset($content)) {
		echo "Error: Not enough info provided to edit the post.";
		header("Refresh:0 url=index.php");
	}

	if(getPostOwner($postid) == $currentUser){
		if(editPost($content, $postid, $currentUser)) {
				echo "<script>alert('Post has been edited!');</script>";
				header("Refresh:0 url=index.php");
			} else {
				echo "<script>alert('Error: You cannot edit this post.');</script>";
				header("Refresh:0 url=index.php");
		}
	} else {
		echo "<script>alert('Error: You do not have permission to delete this post.');</script>";
		header("Refresh:0 url=index.php");
	}



	function editPost($content, $postid, $currentUser) {
		global $mysqli;
		// check if current user matches the owner of the postID
		$prepared_sql = "UPDATE posts SET content=? WHERE postid=? AND owner=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			return FALSE;
		}
		echo "$content, $postid, $currentUser";
		$stmt->bind_param("sis", $content, $postid, $currentUser); // i binds integers?
		if (!$stmt->execute()) { 
			echo "!!!!!!!";
			return FALSE;
		}
		return TRUE;
  	}

  	function sanitize_input($input) {
  		$input = htmlspecialchars($input);
  		return $input;
  	}


  	function getPostOwner($postid,$currentUser) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM posts WHERE postid=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			echo "Prepared Statement Error";
			return FALSE;
		}
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "Error!!!";
			return FALSE;
		}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			/*if (htmlentities($owner) == $currentUser){
				return TRUE;
			} else {
				return FALSE;
			}*/
			return htmlentities($owner);
		} else {
			//return FALSE;
			return NULL;
		}
	}
  	
?>
```
## /registrationform.php
```php
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Registration page - SecAD</title>
</head>
<body>
  <h2>Register a new account</h2>

<?php
  // function sanitize_input($input) {
  //   $input = htmlspecialchars($input);
  //   return $input;
  // }

  // //username input from the user via HTTP Request POST
  // $username = sanitize_input($_POST["username"]);
  // //password input from the user via HTTP Request POST
  // $password = sanitize_input($_POST["password"]);
  // //echo "got username=" . $username . ";password= $password"; //for debug ONLY
?>
          <form action="checkregistration.php" method="POST" class="form login">
          <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
                Username(email):<input type="text" class="text_field" name="username" required
+                   pattern="^[\w.-]+@[\w-]+(.[\w-]+)*$"
+                       title="Please enter a valid email as username" 
                        placeholder="Your email address"
                        onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');"/> <br>
                Password: <input type="password" class="text_field" name="password" required
  +                 pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,}$"
                        placeholder="Your password"
            +           title="Password must have at least 8 characters with 1 special symbol !@#$%^& 1 number,
              +         1 lowercase, and 1 UPPERCASE"
                                  onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');
              +         form.repassword.pattern = this.value;"/>
              +         Retype Password: <input type="password" class="text_field" name="repassword"
                        placeholder="Retype your password" required
              +           title="Password does not match"
                          onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');"/> <br>                
                <button class="button" type="submit">
                  Register
                </button>
          </form>

          <a href="form.php">Already have an account?</a>
</body>
</html>


```
## /createpost.php
```php
<?php
  require "database.php";
  require "session_auth.php";
  $nocsrftoken = $_POST["nocsrftoken"];
    if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
      echo "<script>alert('CSRF is detected!');</script>";
      header("Refresh:0 url=form.php");
      die();
  }
  $content = sanitize_input($_POST["content"]);
  //$postDate = NULL;
  $owner = sanitize_input($_SESSION["username"]); // this should be the username sent to this file (the one posting)


  // if the user submits a blank post, notify them this is not allowed
  if ($content == "") {
    echo "<script>alert('Error: You cannot create an empty post!');</script>";
    //session_destroy();
    header("Refresh:0; url=index.php");
    die();  
  } else {
    // add post to database
    if (addpost($content,$owner)) {
    // provide the user an alert that displays their post was a success
      echo "<script>alert('Post has been created!');</script>";
    //  session_destroy();
      header("Refresh:0; url=index.php");
      die();
    } else {
      echo "<script>alert('Error: Cannot create post!');</script>";
      //session_destroy();
      header("Refresh:0; url=index.php");
      die();
    }
  }


  function addpost($content,$owner) {
    global $mysqli;
    $prepared_sql = "INSERT INTO posts (content,owner)  VALUES (?,?);";
   // echo "DEBUG>prepared_sql=$prepared_sql\n";
    if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
    $stmt->bind_param("ss",$content,$owner);
    if(!$stmt->execute()) return FALSE;
    return TRUE;
  }

  function sanitize_input($input) {
    $input = htmlspecialchars($input);
    return $input;
  }

?>
```
## /post.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';
	$rand= bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["nocsrftoken"] = $rand;

	$postid = sanitize_input($_POST["postid"]);
	//echo "post id: $postid";

	getPost($_POST["postid"]);
	echo "<br><br>";
	echo "\nComment Section: \r\n";
	echo "<br><br>";
	showComments($_POST["postid"]);
?>

<!-- edit post not functional yet -->
	<form action="editpost.php" method="POST">
	        Edit your post in the box below <br>
	        <input type="hidden" name="postid" value="<?php echo  $postid ; ?>" />
	        <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
	        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
	                    pattern=".{1,250}"
	                    title="The post must have between 1 and 250 characters" /> <br>
	       	<button class="button" type="submit">
	       	 Edit Post
	        </button>
	</form>

	<form action="comment.php" method="POST">
        Write a comment in the box below <br>
        <input type="hidden" name="postid" value="<?php echo  $postid; ?>" />
        <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
                    pattern=".{1,250}"
                    title="The comment must have between 1 and 250 characters" /> <br>
         <button class="button" type="submit">
           Add Comment
        </button>
    </form>

<!-- delete not functional yet -->
    <form action="deletepost.php" method="POST">
   	 	<input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
        <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
       	<button class="button" type="submit">
       	 	Delete Post
        </button>
    </form>

<a href="index.php">Home</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>

<?php
	function getPost($postid){
		global $mysqli;
		$prepared_sql = "SELECT owner, postDate, content FROM posts WHERE postid=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql)) {
			echo "Prepared Statement Error";
			return FALSE;
		}
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "!!!!!!!!!!!!!!!!";
			return FALSE;
		}
		$owner = NULL; $postDate = NULL; $content = NULL;
		if(!$stmt->bind_result($owner,$postDate,$content)) echo "Binding Error";
		while($stmt->fetch()){
			echo "Post by '" . htmlentities($owner) . "' at " . htmlentities($postDate) . ": " . htmlentities($content);
		}
	}

	function showComments($postid){
		global $mysqli;
		$prepared_sql = "SELECT owner, content, postDate, commentid FROM comments WHERE DaddyID=?;"; 
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			echo "Prepared Statement Error";
			return FALSE;
		}
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "!!!!!!!!!";
			return FALSE;
		}
		$owner = NULL; $content = NULL; $commentid = NULL;
		if(!$stmt->bind_result($owner,$content,$postDate,$commentid)) echo "Binding Error";
		while($stmt->fetch()){
			echo htmlentities($owner) . " commented at " . htmlentities($postDate) . ": " . htmlentities($content) . "<br><br>";
			if (strcmp($owner, $_SESSION['username']) == 0){

	?>
	<!-- edit comment not functional yet -->
	
<!-- 			<form action="editcomment.php" method="POST">
        		<input type="hidden" name="commentid" value="<?php echo $commentid; ?>" />
       			<button class="button" type="submit">
					 Edit Comment
        		</button>
    		</form> -->
<?php
			}
		}
	}

	function sanitize_input($input) {
  		$input = trim($input);
  		$input = stripslashes($input);
  		$input = htmlspecialchars($input);
  		return $input;
  	}
?>
```
## /deletepost.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';
	$nocsrftoken = $_POST["nocsrftoken"];
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=form.php");
		die();
	}

	$postid = sanitize_input($_POST["postid"]);
	$currentUser = sanitize_input($_SESSION["username"]);

	/*if (!empty($postid) AND !empty($currentUser) AND isset($postid) AND isset($currentUser)) {
		echo "<script>alert('Error: Not enough info to delete post.');</script>";
		header("Refresh:0 url=index.php");
	}*/

	// if it is the user is the owner
	if(getPostOwner($postid) == $currentUser){

		if(deletePost($postid)) {
			//echo "deleted post";
			echo "<script>alert('Post deleted!');</script>";
			header("Refresh:0 url=index.php");
		} else {
			echo "<script>alert('Error: Cannot delete this post.');</script>";
			header("Refresh:0 url=index.php");
		}

		// DELETE COMMENTS FUNCTION NEED TROUBLESHOOTING (can't have multiple prep statements at once?)
		/*
		if(deletePostComments($postid)) {
			echo "<script>alert('Post deleted!');</script>";
			header("Refresh:0 url=index.php");
		} else {
			echo "<script>alert('Error: Cannot delete this post.');</script>";
			header("Refresh:0 url=index.php");
		}*/

	} else {
		echo "<script>alert('Error: You do not have permission to delete this post.');</script>";
		header("Refresh:0 url=index.php");
	}

	function getPostOwner($postid,$currentUser) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM posts WHERE postid=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			echo "Prepared Statement Error";
			return FALSE;
		}
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "Error!!!";
			return FALSE;
		}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			/*if (htmlentities($owner) == $currentUser){
				return TRUE;
			} else {
				return FALSE;
			}*/
			return htmlentities($owner);
		} else {
			//return FALSE;
			return NULL;
		}
	}

	function deletePost($postid) {
		global $mysqli;
		$prepared_sql = "DELETE FROM posts WHERE postid=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			echo "Prepared Statement Error1";
			return FALSE;
		}
		$stmt->bind_param("i", $postid);
		if (!$stmt->execute()) { 
			echo "Error!!1";
			return FALSE;
		}
		return TRUE;
  	}

  	function deletePostComments($postid) {
  		global $mysqli;
		$prepared_sql = "DELETE * FROM comments WHERE DaddyID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			echo "Prepared Statement Error2";
			return FALSE;
		}
		$stmt->bind_param("i", $postid);
		if (!$stmt->execute()) { 
			echo "Error!!2";
			return FALSE;
		}
		return TRUE;
  	}


  	function sanitize_input($input) {
  		$input = htmlspecialchars($input);
  		return $input;
  	}
?>
```
## /comment.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';

	$content = sanitize_input($_POST["content"]);
	$owner = sanitize_input($_SESSION["username"]);
	$DaddyID = sanitize_input($_POST["postid"]); 
	$nocsrftoken = $_POST["nocsrftoken"];
    if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
      echo "<script>alert('CSRF is detected!');</script>";
      header("Refresh:0 url=form.php");
      die();
  }


	//echo "Post ID: $DaddyID";

	// if values aren't empty, then add comment
	if ($content == "") {
		echo "<script>alert('Error: Not enough information provided.');</script>";
		header("Refresh:0 url=index.php");
	} else {
		if(addcomment($content, $owner, $DaddyID)) {
			echo "<script>alert('Comment Added!');</script>";
			header("Refresh:0 url=index.php");
		} else {
			echo "<script>alert('Error: Cannot create this comment.');</script>";
			header("Refresh:0 url=index.php");
		}
	}

	function addcomment($content, $owner, $DaddyID) {
		global $mysqli;
		$prepared_sql = "INSERT INTO comments (content, owner, DaddyID) VALUES (?, ?, ?);";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			//echo "$content , $owner , $DaddyID";
			echo "Prepared Statement Error";
			return FALSE;
		}
		//echo "$content , $owner , $DaddyID";
		$stmt->bind_param("ssi",$content,$owner,$DaddyID); 
		if (!$stmt->execute()) { 
			echo "Binding Error";
			return FALSE;
		}
		return TRUE;
  	}

  	function sanitize_input($input) {
  		$input = htmlspecialchars($input);
  		return $input;
  	}


?>
```
## /database.php
```php
<?php
	$mysqli = new mysqli('localhost','team15user','team15Pword','secadteam15');
	if ($mysqli->connect_errno){
			printf("Database connection failed: %s\n", $mysqli->connect_error);
			exit();
	}

	function securechecklogin($username, $password) {
  		global $mysqli;
		$prepared_sql = "SELECT * FROM users WHERE username= ? " .
						" AND password=password(?);";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ss", $username,$password);
		if(!$stmt->execute()) echo "Execute Error";
		if(!$stmt->store_result()) echo "Store_result Error";
  		//echo "DEBUG>sql= $sql"; return TRUE; // debugging...
  		$result = $stmt;
		if ($result->num_rows ==1)
			return TRUE;
		return FALSE;
  	}

	function addnewuser($newusername, $newpassword) {
  		global $mysqli;
		$prepared_sql = "INSERT INTO users VALUES (?, password(?));";
		//echo "DEBUG>prepared_sql=$prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("ss",$newusername,$newpassword);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
  	}

  	function changepassword($newpassword, $username) {
  		global $mysqli;
		$prepared_sql = "UPDATE users SET password=password(?) WHERE username= ?;";
		//echo "DEBUG>prepared_sql=$prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("ss",$newpassword,$username);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
  	}
?>

```
## /logout.php
```php
<?php
	session_start();
	session_destroy();
?>
<p> You are logged out! </p>

<a href="form.php">Login again</a>
```
## /form.php
```php
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login page - SecAD</title>
</head>
<body>
      	<h1>Team Project, SecAD</h1>
	<h2>Team 15: Jonathan Moran & Nathan Stull

<?php
  // //username input from the user via HTTP Request POST
  // $username = $_POST["username"];
  // //password input from the user via HTTP Request POST
  // $password = $_POST["password"];
  // //echo "got username=" . $username . ";password= $password"; //for debug ONLY
?>
          <form action="index.php" method="POST" class="form login">
                Username:<input type="text" class="text_field" name="username" /> <br>
                Password: <input type="password" class="text_field" name="password" /> <br>
                <button class="button" type="submit">
                  Login
                </button>
          </form>

          <form action="registrationform.php" method="POST" class="form login">
                <button class="button" type="submit">
                  Register
                </button>
          </form>
</body>
</html>


```
## /sessiontest.php
```php
<?php
session_start(); 
if(isset($_SESSION['views']))
    $_SESSION['views'] = $_SESSION['views']+ 1;
else{
    $_SESSION['views'] = 1;
}
echo "You have visited this page " . $_SESSION['views'] . " times"; 
?>

```
## /superuser/form.php
```php
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login page - SecAD</title>
</head>
<body>
      	<h1>Team Project, SecAD</h1>
	<h2>Team 15: Jonathan Moran & Nathan Stull</h2>
  <br>
  <h3>Super User Login</h3>
<?php
  //username input from the user via HTTP Request POST
  //$username = $_POST["username"];
  //password input from the user via HTTP Request POST
  //$password = $_POST["password"];
  //echo "got username=" . $username . ";password= $password"; //for debug ONLY
?>
          <form action="index.php" method="POST" class="form login">
                Username:<input type="text" class="text_field" name="username" /> <br>
                Password: <input type="password" class="text_field" name="password" /> <br>
                <button class="button" type="submit">
                  Login
                </button>
          </form>

          <!-- registration for super users is not required -->
          <!-- <form action="registrationform.php" method="POST" class="form login">
                <button class="button" type="submit">
                  Register
                </button>
          </form> -->
</body>
</html>


```
## /superuser/superdatabase.php
```php
<?php
	$mysqli = new mysqli('localhost','team15user','team15Pword','secadteam15');
	if ($mysqli->connect_errno){
			printf("Database connection failed: %s\n", $mysqli->connect_error);
			exit();
	}

	// superuser's function of secure checklogin
	function securecheckloginsuper($username, $password) {

		global $mysqli;
		$prepared_sql = "SELECT * FROM superusers WHERE username= ? " .
						" AND password=password(?);";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ss", $username, $password);
		if (!$stmt->execute()) 
			echo "Execute Error";
		if (!$stmt->store_result())
			echo "Store Result Error";
		$result = $stmt;
		if ($result->num_rows == 1)
			return TRUE;
		return FALSE; 
  	}

?>
```
## /superuser/registeredusers.php
```php
<?php
	require 'superdatabase.php';
	require "session_auth.php";

	if(strcmp($_SESSION["role"], "superuser") !== 0) {
		echo "<script>alert('You do not have authorization! You must be a super user.'');</script>";
		session_destroy();
		header("Refresh:0; url=index.php");
		die();
	}


?>
	<h2> Please view the registered users below:</h2>
	<br>
	<h3>Regular Users:</h3>
	<br>
<?php
	
	//displaying regular users
	global $mysqli;
	$prepared_sql = "SELECT username FROM users;";
	if (!$stmt = $mysqli->prepare($prepared_sql))
		return FALSE;
	if (!$stmt->execute()) { 
		echo "failed to execute";
		return FALSE;
	}

	$username = NULL;
	if(!$stmt->bind_result($username)) echo "Binding failed";
	while($stmt->fetch()){
		echo "'" . htmlentities($username) . "'<br>";
	}

	echo "<br><h3>SuperUsers:</h3><br>";

	// displaying all superusers
	$prepared_sql = "SELECT username FROM superusers;";
	if (!$stmt = $mysqli->prepare($prepared_sql))
		return FALSE;
	if (!$stmt->execute()) { 
		echo "failed to execute";
		return FALSE;
	}
	$username = NULL;
	if(!$stmt->bind_result($username)) echo "Binding failed";
	while($stmt->fetch()){
		echo "'" . htmlentities($username) . "'<br>";
	}
?>

	<br>
	<a href="index.php">Go back to see posts</a> | <a href="form.php">Logout</a>
```
## /superuser/index.php
```php
<?php
	require 'superdatabase.php';
	$lifetime = 15 * 60;
	$path ="/teamproject/superuser";
	$domain ="*minifacebook.com";
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime,$path,$domain,$secure,$httponly);
	session_start();

  	$username = sanitize_input($_POST["username"]);
  	$password = sanitize_input($_POST["password"]);

	//check if username and password are set
	if (isset($_POST["username"]) and isset($_POST["password"])) { 

		// try to log them in with given information
		if (securecheckloginsuper($username,$password)) {
			$_SESSION["logged"] = TRUE;
			$_SESSION["username"] = $username;
			$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
			$_SESSION["role"] = "superuser"; // different from regular index.php
		} else {
			echo "<script>alert('Invalid username/password');</script>";
			session_destroy();
			header("Refresh:0; url=form.php");
			die();
		}
	}

	// if role does not equal 'superuser'
	if(strcmp($_SESSION["role"], "superuser") !== 0) {
		echo "<script>alert('You do not have authorization! You must be a super user.'');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}

	if (!isset($_SESSION["logged"] ) or $_SESSION["logged"] != TRUE) {
		echo "<script>alert('You have not logged in. Please login first');</script>";
		header("Refresh:0; url=form.php");
		die();
	} 
	if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]) {
		echo "<script>alert('Session hijacking attack is detected!');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}
	
	//if (securecheckloginsuper($_POST["username"],$_POST["password"])) {
?>

	<h2> Welcome <?php echo htmlentities($_SESSION['username']); ?> !</h2>
	<br>

	<a href="registeredusers.php">View registered users</a> | <a href="form.php">Logout</a>
	<br><br>

<?php		

  	global $mysqli;
	$prepared_sql = "SELECT owner, postDate, content, postid FROM posts;";
	if (!$stmt = $mysqli->prepare($prepared_sql)) {
		echo "Prepared Statement Error";
		return FALSE;
	}
	if (!$stmt->execute()) { 
		echo "Execute Error";
		return FALSE;
	}

	if (!isset($_SESSION["logged"] ) or $_SESSION["logged"] != TRUE) {
		echo "<script>alert('You have not loggged in. Please login first');</script>";
		header("Refresh:0; url=form.php");
		die();
	} 
	
	$owner = NULL; $postDate = NULL; $content = NULL;
	if(!$stmt->bind_result($owner, $postDate, $content, $postid)) echo "Binding failed";
	while($stmt->fetch()){
		echo "Post by '" . htmlentities($owner) . "' at " . htmlentities($postDate) . ": " . htmlentities($content);
	?>


  	<form action="/var/www/htlm/teamproject/post.php" method="POST">
  	<input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
        <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
       	<button class="button" type="submit">
           Go to Post
        </button>
    </form>


	<?php
		echo "<br><br><br>";
	}

	function sanitize_input($input) {
    	$input = htmlspecialchars($input);
    	return $input;
  	}
?>
```
## /superuser/session_auth.php
```php
<?php
	$lifetime = 15 * 60;
	$path ="/";
	$domain ="*.minifacebook.com";
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime,$path,$domain,$secure,$httponly);
	session_start();

	//check the session
	if( !isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE){
		//the session is not authenticated
		echo "<script>alert('You have to login first!');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}

	if( $_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
		//it is a session hijacking attacks ince it comes from a diff browser
		echo "<script>alert('Session hijacking attack is detected!');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}
?>
```
## /changepassword.php
```php
<?php
	//Commented out for now: causes session hijacking no matter what.
	require 'database.php';
	require "session_auth.php";
	$username = sanitize_input($_SESSION["username"]);
	$newpassword = sanitize_input($_POST["newpassword"]);
	$nocsrftoken = $_POST["nocsrftoken"];
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=form.php");
		die();
	}
	if (isset($username) AND isset($newpassword)) {
		//echo "DEBUG:changepassword.php->GOT: username=$username;newpassword=$newpassword\n<br>";
		if(changepassword($newpassword,$username)) {
			echo "<h4>The new password has been set.</h4>";
		}else{
			echo "<h4>Error: Cannot change the password.</h4>";
		}
	}else{
		echo "No provided username/password to change.";
		exit();
	}

	function sanitize_input($input) {
  		$input = htmlspecialchars($input);
  		return $input;
  	}
?>
<a href="index.php">Home</a> | <a href="logout.php">Logout</a>
```
## /index.php
```php
<?php
	require 'database.php';
	$lifetime = 15 * 60;
	$path ="/teamproject";
	$domain ="*minifacebook.com";
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime,$path,$domain,$secure,$httponly);
	session_start();
	$rand= bin2hex(openssl_random_pseudo_bytes(16));
  	$_SESSION["nocsrftoken"] = $rand;

  	$username = sanitize_input($_POST["username"]);
  	$password = sanitize_input($_POST["password"]);

	if (isset($_POST["username"]) and isset($_POST["password"]) ){
		if (securechecklogin($username,$password)) {
			$_SESSION["logged"] = TRUE;
			$_SESSION["username"] = $username;
			$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
		}else{
			echo "<script>alert('Invalid username/password');</script>";
			session_destroy();
			header("Refresh:0; url=form.php");
			die();
		}
	}
	if (!isset($_SESSION["logged"] ) or $_SESSION["logged"] != TRUE) {
		echo "<script>alert('You have not loggged in. Please login first');</script>";
		header("Refresh:0; url=form.php");
		die();
	} 
	if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]) {
		echo "<script>alert('Session hijacking attack is detected!');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}
	// if (securechecklogin($_POST["username"],$_POST["password"])) {
?>
	<h2> Welcome <?php echo htmlentities($_SESSION["username"]) ?> !</h2>
	<a href="logout.php">Logout</a>
	<a href="changepasswordform.php">Change your password here</a>
	<br></br>
	<form action="createpost.php" method="POST" class="form post">
				<input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
                Create a new post: <br>
                <textarea id="post" name="content" rows="4" cols="50">What's on your mind?</textarea>
			    <br>
                <button class="button" type="submit">
                  Post
                </button>
    </form>
<?php

	global $mysqli;
	$prepared_sql = "SELECT owner, postDate, content, postid FROM posts;";
	if (!$stmt = $mysqli->prepare($prepared_sql)) {
		echo "Prepared Statement Error";
		return FALSE;
	}
	if (!$stmt->execute()) { 
		echo "Execute Error";
		return FALSE;
	}
	$owner = NULL; $postDate = NULL; $content = NULL;
	if(!$stmt->bind_result($owner, $postDate, $content, $postid)) echo "Binding failed";
	while($stmt->fetch()){
		echo "Post by '" . htmlentities($owner) . "' at " . htmlentities($postDate) . ": " . htmlentities($content);
	?>


  	<form action="post.php" method="POST">
  		<input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
        <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
       	<button class="button" type="submit">
           Go to Post
        </button>
    </form>


<?php
		echo "<br><br><br>";
	}

	function sanitize_input($input) {
  		$input = htmlspecialchars($input);
  		return $input;
  	}


?>

```
## /checkregistration.php
```php
<?php
  require "database.php";

  $username = sanitize_input($_POST["username"]);
  $password = sanitize_input($_POST["password"]);
  $nocsrftoken = $_POST["nocsrftoken"];
  if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
    echo "<script>alert('CSRF is detected!');</script>";
    header("Refresh:0 url=form.php");
    die();
  }

  if (isset($username) and isset($password)){
    //echo "got here1";
    $_SESSION["logged"] = TRUE;

    // if username doesn't already exist, show successfully registered and add username and password to the database
    if ($username == "" || $password == ""){
        echo "<script>alert('Cannot create an account with a blank username and/or password! Try again.');</script>";
        session_destroy();
        header("Refresh:0; url=registrationform.php");
        die();
    } else if (!usernameexists($username)) {
      //$_SESSION["username"] = $_POST["username"];
      //$_SESSION["password"] = $_POST["password"];

      // insert the username and password into the database
      if (addnewuser($username, $password)) {
        // provide the user an alert that displays their registration was a success
        echo "<script>alert('Congratulations! You have successfull created a new account and can login!');</script>";
        session_destroy();
        header("Refresh:0; url=form.php");
        die();
      } else {
        echo "<script>alert('Error: Cannont create new account');</script>";
        session_destroy();
        header("Refresh:0; url=registrationform.php");
        die();
      }

    // if username does exists, tell the user it already exists and they have to try registering again
    } else {
      echo "<script>alert('An account with that username already exists. Try again.');</script>";
      session_destroy();
      header("Refresh:0; url=registrationform.php");
      die();      
    }
  } 


  if (!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE) {
    echo "<script>alert('You haven't registered. Please register first.');</script>";
    header("Refresh:0; url=registrationform.php");
    die();
  } 

  function usernameexists($username) {
    global $mysqli;
    $prepared_sql = "SELECT * FROM users WHERE username= ? " .";";
    if(!$stmt = $mysqli->prepare($prepared_sql)) {
      echo "Prepared Statement Error";
    }
    $stmt->bind_param("s", $username);
    if(!$stmt->execute()) echo "Execute Error";
    if(!$stmt->store_result()) echo "Store_result Error";
    $result = $stmt;
    if ($result->num_rows>0) return TRUE;
    return FALSE;
  }

  function sanitize_input($input) {
      $input = htmlspecialchars($input);
      return $input;
  }
?>
```
## /session_auth.php
```php
<?php
	$lifetime = 15 * 60;
	$path ="/teamproject";
	$domain ="*minifacebook.com";
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime,$path,$domain,$secure,$httponly);
	session_start();

	//check the session
	if( !isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE){
		//the session is not authenticated
		echo "<script>alert('You have to login first!');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}

	if( $_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
		//it is a session hijacking attacks ince it comes from a diff browser
		echo "<script>alert('Session hijacking attack is detected!');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}
?>
```
## /changepasswordform.php
```php
<?php
  require "session_auth.php";
  $rand= bin2hex(openssl_random_pseudo_bytes(16));
  $_SESSION["nocsrftoken"] = $rand;
  
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Change Password Page - SecAD</title>
</head>
<body>
  <h1>Change Password, SecAD</h1>

<?php
  echo "Current time: " . date("Y-m-d h:i:sa");
?>
          <form action="changepassword.php" method="POST" class="form login">
                Username:<!--input type="text" class="text_field" name="username" / --> 
                <?php echo htmlentities($_SESSION["username"]); ?>
                <br>
                <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
                New password: <input type="password" class="text_field" name="newpassword" /> <br>
                <button class="button" type="submit">
                  Change password
                </button>
          </form>

          <a href="index.php">Home</a> | <a href="logout.php">Logout</a>

</body>
</html>

```
