<?php
	//Commented out for now: causes session hijacking no matter what.
	require 'database.php';
	require "session_auth.php";
	$username = sanitize_input($_SESSION["username"]);
	$newpassword = sanitize_input($_POST["newpassword"]);
	$nocsrftoken = $_POST["nocsrftoken"];
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=form.php");
		die();
	}
	if (isset($username) AND isset($newpassword)) {
		//echo "DEBUG:changepassword.php->GOT: username=$username;newpassword=$newpassword\n<br>";
		if(changepassword($newpassword,$username)) {
			echo "<h4>The new password has been set.</h4>";
		}else{
			echo "<h4>Error: Cannot change the password.</h4>";
		}
	}else{
		echo "No provided username/password to change.";
		exit();
	}

	function sanitize_input($input) {
  		$input = htmlspecialchars($input);
  		return $input;
  	}
?>
<a href="index.php">Home</a> | <a href="logout.php">Logout</a>