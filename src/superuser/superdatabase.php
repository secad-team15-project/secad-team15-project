<?php
	$mysqli = new mysqli('localhost','team15user','team15Pword','secadteam15');
	if ($mysqli->connect_errno){
			printf("Database connection failed: %s\n", $mysqli->connect_error);
			exit();
	}

	// superuser's function of secure checklogin
	function securecheckloginsuper($username, $password) {

		global $mysqli;
		$prepared_sql = "SELECT * FROM superusers WHERE username= ? " .
						" AND password=password(?);";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ss", $username, $password);
		if (!$stmt->execute()) 
			echo "Execute Error";
		if (!$stmt->store_result())
			echo "Store Result Error";
		$result = $stmt;
		if ($result->num_rows == 1)
			return TRUE;
		return FALSE; 
  	}

?>