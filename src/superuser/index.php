<?php
	require 'superdatabase.php';
	$lifetime = 15 * 60;
	$path ="/teamproject/superuser";
	$domain ="*minifacebook.com";
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime,$path,$domain,$secure,$httponly);
	session_start();

  	$username = sanitize_input($_POST["username"]);
  	$password = sanitize_input($_POST["password"]);

	//check if username and password are set
	if (isset($_POST["username"]) and isset($_POST["password"])) { 

		// try to log them in with given information
		if (securecheckloginsuper($username,$password)) {
			$_SESSION["logged"] = TRUE;
			$_SESSION["username"] = $username;
			$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
			$_SESSION["role"] = "superuser"; // different from regular index.php
		} else {
			echo "<script>alert('Invalid username/password');</script>";
			session_destroy();
			header("Refresh:0; url=form.php");
			die();
		}
	}

	// if role does not equal 'superuser'
	if(strcmp($_SESSION["role"], "superuser") !== 0) {
		echo "<script>alert('You do not have authorization! You must be a super user.'');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}

	if (!isset($_SESSION["logged"] ) or $_SESSION["logged"] != TRUE) {
		echo "<script>alert('You have not logged in. Please login first');</script>";
		header("Refresh:0; url=form.php");
		die();
	} 
	if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]) {
		echo "<script>alert('Session hijacking attack is detected!');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}
	
	//if (securecheckloginsuper($_POST["username"],$_POST["password"])) {
?>

	<h2> Welcome <?php echo htmlentities($_SESSION['username']); ?> !</h2>
	<br>

	<a href="registeredusers.php">View registered users</a> | <a href="form.php">Logout</a>
	<br><br>

<?php		

  	global $mysqli;
	$prepared_sql = "SELECT owner, postDate, content, postid FROM posts;";
	if (!$stmt = $mysqli->prepare($prepared_sql)) {
		echo "Prepared Statement Error";
		return FALSE;
	}
	if (!$stmt->execute()) { 
		echo "Execute Error";
		return FALSE;
	}

	if (!isset($_SESSION["logged"] ) or $_SESSION["logged"] != TRUE) {
		echo "<script>alert('You have not loggged in. Please login first');</script>";
		header("Refresh:0; url=form.php");
		die();
	} 
	
	$owner = NULL; $postDate = NULL; $content = NULL;
	if(!$stmt->bind_result($owner, $postDate, $content, $postid)) echo "Binding failed";
	while($stmt->fetch()){
		echo "Post by '" . htmlentities($owner) . "' at " . htmlentities($postDate) . ": " . htmlentities($content);
	?>


  	<form action="/var/www/htlm/teamproject/post.php" method="POST">
  	<input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
        <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
       	<button class="button" type="submit">
           Go to Post
        </button>
    </form>


	<?php
		echo "<br><br><br>";
	}

	function sanitize_input($input) {
    	$input = htmlspecialchars($input);
    	return $input;
  	}
?>