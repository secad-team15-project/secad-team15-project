<?php
	require 'superdatabase.php';
	require "session_auth.php";

	if(strcmp($_SESSION["role"], "superuser") !== 0) {
		echo "<script>alert('You do not have authorization! You must be a super user.'');</script>";
		session_destroy();
		header("Refresh:0; url=index.php");
		die();
	}


?>
	<h2> Please view the registered users below:</h2>
	<br>
	<h3>Regular Users:</h3>
	<br>
<?php
	
	//displaying regular users
	global $mysqli;
	$prepared_sql = "SELECT username FROM users;";
	if (!$stmt = $mysqli->prepare($prepared_sql))
		return FALSE;
	if (!$stmt->execute()) { 
		echo "failed to execute";
		return FALSE;
	}

	$username = NULL;
	if(!$stmt->bind_result($username)) echo "Binding failed";
	while($stmt->fetch()){
		echo "'" . htmlentities($username) . "'<br>";
	}

	echo "<br><h3>SuperUsers:</h3><br>";

	// displaying all superusers
	$prepared_sql = "SELECT username FROM superusers;";
	if (!$stmt = $mysqli->prepare($prepared_sql))
		return FALSE;
	if (!$stmt->execute()) { 
		echo "failed to execute";
		return FALSE;
	}
	$username = NULL;
	if(!$stmt->bind_result($username)) echo "Binding failed";
	while($stmt->fetch()){
		echo "'" . htmlentities($username) . "'<br>";
	}
?>

	<br>
	<a href="index.php">Go back to see posts</a> | <a href="form.php">Logout</a>