<?php
	require 'database.php';
	require 'session_auth.php';

	$content = sanitize_input($_POST["content"]);
	$owner = sanitize_input($_SESSION["username"]);
	$DaddyID = sanitize_input($_POST["postid"]); 
	$nocsrftoken = $_POST["nocsrftoken"];
    if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
      echo "<script>alert('CSRF is detected!');</script>";
      header("Refresh:0 url=form.php");
      die();
  }


	//echo "Post ID: $DaddyID";

	// if values aren't empty, then add comment
	if ($content == "") {
		echo "<script>alert('Error: Not enough information provided.');</script>";
		header("Refresh:0 url=index.php");
	} else {
		if(addcomment($content, $owner, $DaddyID)) {
			echo "<script>alert('Comment Added!');</script>";
			header("Refresh:0 url=index.php");
		} else {
			echo "<script>alert('Error: Cannot create this comment.');</script>";
			header("Refresh:0 url=index.php");
		}
	}

	function addcomment($content, $owner, $DaddyID) {
		global $mysqli;
		$prepared_sql = "INSERT INTO comments (content, owner, DaddyID) VALUES (?, ?, ?);";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			//echo "$content , $owner , $DaddyID";
			echo "Prepared Statement Error";
			return FALSE;
		}
		//echo "$content , $owner , $DaddyID";
		$stmt->bind_param("ssi",$content,$owner,$DaddyID); 
		if (!$stmt->execute()) { 
			echo "Binding Error";
			return FALSE;
		}
		return TRUE;
  	}

  	function sanitize_input($input) {
  		$input = htmlspecialchars($input);
  		return $input;
  	}


?>