<?php
  require "database.php";

  $username = sanitize_input($_POST["username"]);
  $password = sanitize_input($_POST["password"]);
  $nocsrftoken = $_POST["nocsrftoken"];
  if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
    echo "<script>alert('CSRF is detected!');</script>";
    header("Refresh:0 url=form.php");
    die();
  }

  if (isset($username) and isset($password)){
    //echo "got here1";
    $_SESSION["logged"] = TRUE;

    // if username doesn't already exist, show successfully registered and add username and password to the database
    if ($username == "" || $password == ""){
        echo "<script>alert('Cannot create an account with a blank username and/or password! Try again.');</script>";
        session_destroy();
        header("Refresh:0; url=registrationform.php");
        die();
    } else if (!usernameexists($username)) {
      //$_SESSION["username"] = $_POST["username"];
      //$_SESSION["password"] = $_POST["password"];

      // insert the username and password into the database
      if (addnewuser($username, $password)) {
        // provide the user an alert that displays their registration was a success
        echo "<script>alert('Congratulations! You have successfull created a new account and can login!');</script>";
        session_destroy();
        header("Refresh:0; url=form.php");
        die();
      } else {
        echo "<script>alert('Error: Cannont create new account');</script>";
        session_destroy();
        header("Refresh:0; url=registrationform.php");
        die();
      }

    // if username does exists, tell the user it already exists and they have to try registering again
    } else {
      echo "<script>alert('An account with that username already exists. Try again.');</script>";
      session_destroy();
      header("Refresh:0; url=registrationform.php");
      die();      
    }
  } 


  if (!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE) {
    echo "<script>alert('You haven't registered. Please register first.');</script>";
    header("Refresh:0; url=registrationform.php");
    die();
  } 

  function usernameexists($username) {
    global $mysqli;
    $prepared_sql = "SELECT * FROM users WHERE username= ? " .";";
    if(!$stmt = $mysqli->prepare($prepared_sql)) {
      echo "Prepared Statement Error";
    }
    $stmt->bind_param("s", $username);
    if(!$stmt->execute()) echo "Execute Error";
    if(!$stmt->store_result()) echo "Store_result Error";
    $result = $stmt;
    if ($result->num_rows>0) return TRUE;
    return FALSE;
  }

  function sanitize_input($input) {
      $input = htmlspecialchars($input);
      return $input;
  }
?>