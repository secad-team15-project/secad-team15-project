<?php
  require "database.php";
  require "session_auth.php";
  $nocsrftoken = $_POST["nocsrftoken"];
    if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
      echo "<script>alert('CSRF is detected!');</script>";
      header("Refresh:0 url=form.php");
      die();
  }
  $content = sanitize_input($_POST["content"]);
  //$postDate = NULL;
  $owner = sanitize_input($_SESSION["username"]); // this should be the username sent to this file (the one posting)


  // if the user submits a blank post, notify them this is not allowed
  if ($content == "") {
    echo "<script>alert('Error: You cannot create an empty post!');</script>";
    //session_destroy();
    header("Refresh:0; url=index.php");
    die();  
  } else {
    // add post to database
    if (addpost($content,$owner)) {
    // provide the user an alert that displays their post was a success
      echo "<script>alert('Post has been created!');</script>";
    //  session_destroy();
      header("Refresh:0; url=index.php");
      die();
    } else {
      echo "<script>alert('Error: Cannot create post!');</script>";
      //session_destroy();
      header("Refresh:0; url=index.php");
      die();
    }
  }


  function addpost($content,$owner) {
    global $mysqli;
    $prepared_sql = "INSERT INTO posts (content,owner)  VALUES (?,?);";
   // echo "DEBUG>prepared_sql=$prepared_sql\n";
    if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
    $stmt->bind_param("ss",$content,$owner);
    if(!$stmt->execute()) return FALSE;
    return TRUE;
  }

  function sanitize_input($input) {
    $input = htmlspecialchars($input);
    return $input;
  }

?>