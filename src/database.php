<?php
	$mysqli = new mysqli('localhost','team15user','team15Pword','secadteam15');
	if ($mysqli->connect_errno){
			printf("Database connection failed: %s\n", $mysqli->connect_error);
			exit();
	}

	function securechecklogin($username, $password) {
  		global $mysqli;
		$prepared_sql = "SELECT * FROM users WHERE username= ? " .
						" AND password=password(?);";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ss", $username,$password);
		if(!$stmt->execute()) echo "Execute Error";
		if(!$stmt->store_result()) echo "Store_result Error";
  		//echo "DEBUG>sql= $sql"; return TRUE; // debugging...
  		$result = $stmt;
		if ($result->num_rows ==1)
			return TRUE;
		return FALSE;
  	}

	function addnewuser($newusername, $newpassword) {
  		global $mysqli;
		$prepared_sql = "INSERT INTO users VALUES (?, password(?));";
		//echo "DEBUG>prepared_sql=$prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("ss",$newusername,$newpassword);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
  	}

  	function changepassword($newpassword, $username) {
  		global $mysqli;
		$prepared_sql = "UPDATE users SET password=password(?) WHERE username= ?;";
		//echo "DEBUG>prepared_sql=$prepared_sql\n";
		if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
		$stmt->bind_param("ss",$newpassword,$username);
		if(!$stmt->execute()) return FALSE;
		return TRUE;
  	}
?>
