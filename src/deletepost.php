<?php
	require 'database.php';
	require 'session_auth.php';
	$nocsrftoken = $_POST["nocsrftoken"];
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=form.php");
		die();
	}

	$postid = sanitize_input($_POST["postid"]);
	$currentUser = sanitize_input($_SESSION["username"]);

	/*if (!empty($postid) AND !empty($currentUser) AND isset($postid) AND isset($currentUser)) {
		echo "<script>alert('Error: Not enough info to delete post.');</script>";
		header("Refresh:0 url=index.php");
	}*/

	// if it is the user is the owner
	if(getPostOwner($postid) == $currentUser){

		if(deletePost($postid)) {
			//echo "deleted post";
			echo "<script>alert('Post deleted!');</script>";
			header("Refresh:0 url=index.php");
		} else {
			echo "<script>alert('Error: Cannot delete this post.');</script>";
			header("Refresh:0 url=index.php");
		}

		// DELETE COMMENTS FUNCTION NEED TROUBLESHOOTING (can't have multiple prep statements at once?)
		/*
		if(deletePostComments($postid)) {
			echo "<script>alert('Post deleted!');</script>";
			header("Refresh:0 url=index.php");
		} else {
			echo "<script>alert('Error: Cannot delete this post.');</script>";
			header("Refresh:0 url=index.php");
		}*/

	} else {
		echo "<script>alert('Error: You do not have permission to delete this post.');</script>";
		header("Refresh:0 url=index.php");
	}

	function getPostOwner($postid,$currentUser) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM posts WHERE postid=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			echo "Prepared Statement Error";
			return FALSE;
		}
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "Error!!!";
			return FALSE;
		}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			/*if (htmlentities($owner) == $currentUser){
				return TRUE;
			} else {
				return FALSE;
			}*/
			return htmlentities($owner);
		} else {
			//return FALSE;
			return NULL;
		}
	}

	function deletePost($postid) {
		global $mysqli;
		$prepared_sql = "DELETE FROM posts WHERE postid=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			echo "Prepared Statement Error1";
			return FALSE;
		}
		$stmt->bind_param("i", $postid);
		if (!$stmt->execute()) { 
			echo "Error!!1";
			return FALSE;
		}
		return TRUE;
  	}

  	function deletePostComments($postid) {
  		global $mysqli;
		$prepared_sql = "DELETE * FROM comments WHERE DaddyID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			echo "Prepared Statement Error2";
			return FALSE;
		}
		$stmt->bind_param("i", $postid);
		if (!$stmt->execute()) { 
			echo "Error!!2";
			return FALSE;
		}
		return TRUE;
  	}


  	function sanitize_input($input) {
  		$input = htmlspecialchars($input);
  		return $input;
  	}
?>