-- if the table exists, delete it
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `posts`;
DROP TABLE IF EXISTS `comments`;
DROP TABLE IF EXISTS `superusers`;

-- create a new table
CREATE TABLE `users`(
	username varchar(50) PRIMARY KEY,
	password varchar(100) NOT NULL
);

CREATE TABLE `posts`(
	postid int NOT NULL AUTO_INCREMENT,
	content varchar(500) NOT NULL,
	postDate datetime default now(),
	`owner` varchar(50),
	FOREIGN KEY (`owner`) REFERENCES `users`(`username`) ON DELETE CASCADE,
	PRIMARY KEY (postid)
);

CREATE TABLE `comments`(
	commentid int NOT NULL AUTO_INCREMENT,
	`DaddyID` int,
	FOREIGN KEY (`DaddyID`) REFERENCES `posts`(`postid`) ON DELETE CASCADE,
	content varchar(500) NOT NULL,
	postDate datetime default now(),
	`owner` varchar(50),
	FOREIGN KEY (`owner`) REFERENCES `users`(`username`) ON DELETE CASCADE,
	PRIMARY KEY (commentid)
);

-- create a new table
CREATE TABLE `superusers`(
	username varchar(50) PRIMARY KEY,
	password varchar(100) NOT NULL
);

-- insert data to the table users
LOCK TABLES `users` WRITE;
INSERT INTO `users` VALUES ('admin',password('adminPword'));
UNLOCK TABLES;

-- insert data to the table superusers
LOCK TABLES `superusers` WRITE;
INSERT INTO `superusers` VALUES ('superadmin',password('superadminPword'));
UNLOCK TABLES;