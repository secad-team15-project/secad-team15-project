<?php
	require 'database.php';
	$lifetime = 15 * 60;
	$path ="/teamproject";
	$domain ="*minifacebook.com";
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime,$path,$domain,$secure,$httponly);
	session_start();
	$rand= bin2hex(openssl_random_pseudo_bytes(16));
  	$_SESSION["nocsrftoken"] = $rand;

  	$username = sanitize_input($_POST["username"]);
  	$password = sanitize_input($_POST["password"]);

	if (isset($_POST["username"]) and isset($_POST["password"]) ){
		if (securechecklogin($username,$password)) {
			$_SESSION["logged"] = TRUE;
			$_SESSION["username"] = $username;
			$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
		}else{
			echo "<script>alert('Invalid username/password');</script>";
			session_destroy();
			header("Refresh:0; url=form.php");
			die();
		}
	}
	if (!isset($_SESSION["logged"] ) or $_SESSION["logged"] != TRUE) {
		echo "<script>alert('You have not loggged in. Please login first');</script>";
		header("Refresh:0; url=form.php");
		die();
	} 
	if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]) {
		echo "<script>alert('Session hijacking attack is detected!');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}
	// if (securechecklogin($_POST["username"],$_POST["password"])) {
?>
	<h2> Welcome <?php echo htmlentities($_SESSION["username"]) ?> !</h2>
	<a href="logout.php">Logout</a>
	<a href="changepasswordform.php">Change your password here</a>
	<br></br>
	<form action="createpost.php" method="POST" class="form post">
				<input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
                Create a new post: <br>
                <textarea id="post" name="content" rows="4" cols="50">What's on your mind?</textarea>
			    <br>
                <button class="button" type="submit">
                  Post
                </button>
    </form>
<?php

	global $mysqli;
	$prepared_sql = "SELECT owner, postDate, content, postid FROM posts;";
	if (!$stmt = $mysqli->prepare($prepared_sql)) {
		echo "Prepared Statement Error";
		return FALSE;
	}
	if (!$stmt->execute()) { 
		echo "Execute Error";
		return FALSE;
	}
	$owner = NULL; $postDate = NULL; $content = NULL;
	if(!$stmt->bind_result($owner, $postDate, $content, $postid)) echo "Binding failed";
	while($stmt->fetch()){
		echo "Post by '" . htmlentities($owner) . "' at " . htmlentities($postDate) . ": " . htmlentities($content);
	?>


  	<form action="post.php" method="POST">
  		<input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
        <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
       	<button class="button" type="submit">
           Go to Post
        </button>
    </form>


<?php
		echo "<br><br><br>";
	}

	function sanitize_input($input) {
  		$input = htmlspecialchars($input);
  		return $input;
  	}


?>
