<?php
	require 'database.php';
	require 'session_auth.php';
	$rand= bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["nocsrftoken"] = $rand;

	$postid = sanitize_input($_POST["postid"]);
	//echo "post id: $postid";

	getPost($_POST["postid"]);
	echo "<br><br>";
	echo "\nComment Section: \r\n";
	echo "<br><br>";
	showComments($_POST["postid"]);
?>

<!-- edit post not functional yet -->
	<form action="editpost.php" method="POST">
	        Edit your post in the box below <br>
	        <input type="hidden" name="postid" value="<?php echo  $postid ; ?>" />
	        <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
	        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
	                    pattern=".{1,250}"
	                    title="The post must have between 1 and 250 characters" /> <br>
	       	<button class="button" type="submit">
	       	 Edit Post
	        </button>
	</form>

	<form action="comment.php" method="POST">
        Write a comment in the box below <br>
        <input type="hidden" name="postid" value="<?php echo  $postid; ?>" />
        <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
                    pattern=".{1,250}"
                    title="The comment must have between 1 and 250 characters" /> <br>
         <button class="button" type="submit">
           Add Comment
        </button>
    </form>

<!-- delete not functional yet -->
    <form action="deletepost.php" method="POST">
   	 	<input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
        <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
       	<button class="button" type="submit">
       	 	Delete Post
        </button>
    </form>

<a href="index.php">Home</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>

<?php
	function getPost($postid){
		global $mysqli;
		$prepared_sql = "SELECT owner, postDate, content FROM posts WHERE postid=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql)) {
			echo "Prepared Statement Error";
			return FALSE;
		}
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "!!!!!!!!!!!!!!!!";
			return FALSE;
		}
		$owner = NULL; $postDate = NULL; $content = NULL;
		if(!$stmt->bind_result($owner,$postDate,$content)) echo "Binding Error";
		while($stmt->fetch()){
			echo "Post by '" . htmlentities($owner) . "' at " . htmlentities($postDate) . ": " . htmlentities($content);
		}
	}

	function showComments($postid){
		global $mysqli;
		$prepared_sql = "SELECT owner, content, postDate, commentid FROM comments WHERE DaddyID=?;"; 
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			echo "Prepared Statement Error";
			return FALSE;
		}
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "!!!!!!!!!";
			return FALSE;
		}
		$owner = NULL; $content = NULL; $commentid = NULL;
		if(!$stmt->bind_result($owner,$content,$postDate,$commentid)) echo "Binding Error";
		while($stmt->fetch()){
			echo htmlentities($owner) . " commented at " . htmlentities($postDate) . ": " . htmlentities($content) . "<br><br>";
			if (strcmp($owner, $_SESSION['username']) == 0){

	?>
	<!-- edit comment not functional yet -->
	
<!-- 			<form action="editcomment.php" method="POST">
        		<input type="hidden" name="commentid" value="<?php echo $commentid; ?>" />
       			<button class="button" type="submit">
					 Edit Comment
        		</button>
    		</form> -->
<?php
			}
		}
	}

	function sanitize_input($input) {
  		$input = trim($input);
  		$input = stripslashes($input);
  		$input = htmlspecialchars($input);
  		return $input;
  	}
?>