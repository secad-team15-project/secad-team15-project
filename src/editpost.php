<?php
	require 'database.php';
	require 'session_auth.php';

	$postid = sanitize_input($_POST["postid"]);
	$content = sanitize_input($_POST["content"]); // new content
	$currentUser = sanitize_input($_SESSION["username"]);
	$nocsrftoken = $_POST["nocsrftoken"];
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=form.php");
		die();
	}

	if (empty($postid) || empty($content) || !isset($postid) || !isset($content)) {
		echo "Error: Not enough info provided to edit the post.";
		header("Refresh:0 url=index.php");
	}

	if(getPostOwner($postid) == $currentUser){
		if(editPost($content, $postid, $currentUser)) {
				echo "<script>alert('Post has been edited!');</script>";
				header("Refresh:0 url=index.php");
			} else {
				echo "<script>alert('Error: You cannot edit this post.');</script>";
				header("Refresh:0 url=index.php");
		}
	} else {
		echo "<script>alert('Error: You do not have permission to delete this post.');</script>";
		header("Refresh:0 url=index.php");
	}



	function editPost($content, $postid, $currentUser) {
		global $mysqli;
		// check if current user matches the owner of the postID
		$prepared_sql = "UPDATE posts SET content=? WHERE postid=? AND owner=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			return FALSE;
		}
		echo "$content, $postid, $currentUser";
		$stmt->bind_param("sis", $content, $postid, $currentUser); // i binds integers?
		if (!$stmt->execute()) { 
			echo "!!!!!!!";
			return FALSE;
		}
		return TRUE;
  	}

  	function sanitize_input($input) {
  		$input = htmlspecialchars($input);
  		return $input;
  	}


  	function getPostOwner($postid,$currentUser) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM posts WHERE postid=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			echo "Prepared Statement Error";
			return FALSE;
		}
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "Error!!!";
			return FALSE;
		}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			/*if (htmlentities($owner) == $currentUser){
				return TRUE;
			} else {
				return FALSE;
			}*/
			return htmlentities($owner);
		} else {
			//return FALSE;
			return NULL;
		}
	}
  	
?>