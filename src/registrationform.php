<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Registration page - SecAD</title>
</head>
<body>
  <h2>Register a new account</h2>

<?php
  // function sanitize_input($input) {
  //   $input = htmlspecialchars($input);
  //   return $input;
  // }

  // //username input from the user via HTTP Request POST
  // $username = sanitize_input($_POST["username"]);
  // //password input from the user via HTTP Request POST
  // $password = sanitize_input($_POST["password"]);
  // //echo "got username=" . $username . ";password= $password"; //for debug ONLY
?>
          <form action="checkregistration.php" method="POST" class="form login">
          <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
                Username(email):<input type="text" class="text_field" name="username" required
+                   pattern="^[\w.-]+@[\w-]+(.[\w-]+)*$"
+                       title="Please enter a valid email as username" 
                        placeholder="Your email address"
                        onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');"/> <br>
                Password: <input type="password" class="text_field" name="password" required
  +                 pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,}$"
                        placeholder="Your password"
            +           title="Password must have at least 8 characters with 1 special symbol !@#$%^& 1 number,
              +         1 lowercase, and 1 UPPERCASE"
                                  onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');
              +         form.repassword.pattern = this.value;"/>
              +         Retype Password: <input type="password" class="text_field" name="repassword"
                        placeholder="Retype your password" required
              +           title="Password does not match"
                          onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');"/> <br>                
                <button class="button" type="submit">
                  Register
                </button>
          </form>

          <a href="form.php">Already have an account?</a>
</body>
</html>

